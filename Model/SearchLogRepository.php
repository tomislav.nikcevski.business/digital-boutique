<?php

declare(strict_types=1);

namespace DigitalBoutique\Redirector\Model;

use DigitalBoutique\Redirector\Api\SearchLogRepositoryInterface;
use DigitalBoutique\Redirector\Model\ResourceModel\SearchLog as SearchLogResource;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class SearchLogRepository implements SearchLogRepositoryInterface
{
    /**
     * @var SearchLog[]
     */
    private $cacheById = [];

    /**
     * @var SearchLogFactory
     */
    protected $searchLogFactory;

    /**
     * @var SearchLogResource
     */
    protected $searchLogResource;

    /**
     * SearchLogRepository constructor.
     * @param SearchLogFactory $searchLogFactory
     * @param SearchLogResource $searchLogResource
     */
    public function __construct(
        SearchLogFactory $searchLogFactory,
        SearchLogResource $searchLogResource
    ) {
        $this->searchLogFactory = $searchLogFactory;
        $this->searchLogResource = $searchLogResource;
    }

    /**
     * @param mixed $id
     * @return SearchLog
     * @throws NoSuchEntityException
     */
    public function get($id): SearchLog
    {
        if (isset($this->cacheById[$id])) {
            return $this->cacheById[$id];
        }

        $searchLog = $this->searchLogFactory->create();
        $this->searchLogResource->load($searchLog, $id);

        if (null === $searchLog->getId()) {
            throw new NoSuchEntityException(__('Search log Id: "%1" does not exist.', $id));
        }

        return $this->cacheById[$id] = $searchLog;
    }

    /**
     * @param SearchLog $searchLog
     * @return SearchLog
     * @throws CouldNotSaveException
     */
    public function save(SearchLog $searchLog): SearchLog
    {
        try {
            $this->searchLogResource->save($searchLog);
            return $this->get($searchLog->getId());
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the search log: %1', $exception->getMessage()),
                $exception
            );
        }
    }

    /**
     * @param SearchLog $searchLog
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(SearchLog $searchLog): bool
    {
        try {
            $this->searchLogResource->delete($searchLog);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __('Could not delete the search log: %1.', $exception->getMessage()),
                $exception
            );
        }

        return true;
    }
}
