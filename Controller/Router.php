<?php

declare(strict_types=1);

namespace DigitalBoutique\Redirector\Controller;

use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\Route\ConfigInterface;
use Magento\Framework\App\Router\ActionList;
use Magento\Framework\App\RouterInterface;

/**
 * Class Router
 *
 * This is for checking digital-boutique/m2-test url only
 * Other way to implement it would be to create cms page for m2-test form
 */
class Router implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    protected $actionFactory;

    /**
     * @var ActionList
     */
    private ActionList $actionList;

    /**
     * @var ConfigInterface
     */
    private ConfigInterface $routeConfig;

    /**
     * Router constructor.
     * @param ActionFactory $actionFactory
     * @param ActionList $actionList
     * @param ConfigInterface $routeConfig
     */
    public function __construct(
        ActionFactory $actionFactory,
        ActionList $actionList,
        ConfigInterface $routeConfig
    ) {
        $this->actionFactory = $actionFactory;
        $this->actionList = $actionList;
        $this->routeConfig = $routeConfig;
    }

    /**
     * @param \Magento\Framework\App\RequestInterface $request
     * @return \Magento\Framework\App\ActionInterface|null
     */
    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        $identifier = trim($request->getPathInfo(), '/');
        if (strpos($identifier, "digital-boutique/m2-test") !== 0) {
            return null;
        }

        $modules = $this->routeConfig->getModulesByFrontName('digital-boutique');
        if (empty($modules)) {
            return null;
        }

        $actionClassName = $this->actionList->get($modules[0], null, 'product', 'index');
        return $this->actionFactory->create($actionClassName);
    }
}
