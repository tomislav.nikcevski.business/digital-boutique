__Task__

___Overview___

This is a technical test to gauge the skills and knowledge of Magento 2 backend developer candidates at Digital Boutique.
Requirements:

Create a custom Magento 2 module that handles the following functional requirements:
- Provides a custom page accessible by the path /digital-boutique/m2-test
- here should be a form present on the page that has a single text input field and a submit button
- The text input field should be labeled "Product Sku"
- Upon posting the user should be redirected to a product page if there is a matching sku found
- If no matching sku is found, the same page should be loaded with a notification message

Optional
- Create a custom database table to log all form entries
- Documentation
- Automated Test Coverage

__Comments - Dev__

- Assignment done on Magento 2.4.3, Luma theme with generated fixtures (I had this one set up). Although it should work on latest version.
- Due to form having url m2-test, solution couldn't be just using controllers but router needed to be implemented if solution is strictly in code
- Better and easier alternative to using Router would be to create custom page in admin with url digital-boutique/m2-test/ and form in content, but I didn't want to create page using data patch for this
- Added database logging and documentation, did not have time for test.
